<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Home</title>
   <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
</head>

<body>
   <div class="jumbotron">
      <h1 class="display-4">Hello, world!</h1>
      <p class="lead">This a simple dashboard.</p>
      <hr class="my-4">
      <a class="btn btn-primary btn-lg" href="add_user.html" role="button">Add User</a>
   </div>




   <table class="table">
      <thead>
         <tr>
            <th scope="col">#</th>
            <th scope="col">name</th>
            <th scope="col">email</th>
            <th scope="col">age</th>
         </tr>
      </thead>
      <tbody>
         <?php
require_once 'db.php';
$con = connect();
require_once 'user.php';
$users = getAll($con);
foreach ($users as $key => $u) {
    echo " <tr>";
    echo "    <th scope='row'>" . $u['id'] . "</th>";
    echo "    <td>" . $u['name'] . "</td>";
    echo "    <td>" . $u['email'] . "</td>";
    echo "    <td>" . $u['age'] . "</td>";
    echo " </tr>";

}
disconnect($con);

?>
      </tbody>
   </table>
</body>

</html>