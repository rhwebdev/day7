<?php

function connect()
{
// Create connection
    $conn = mysqli_connect('localhost', 'root', 'root', 'test_db', 8889);
// Check connection
    if (!$conn) {
        // die("Connection failed: " . mysqli_connect_error());
        return false;
    }
    return $conn;
}

function disconnect($conn)
{
    mysqli_close($conn);
}
