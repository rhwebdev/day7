<?php
function requestHandler($request)
{
    $errors = [];
    if (!empty($request)) {
        foreach ($request as $key => $value) {
            if (empty($value)) {
                array_push($errors, $key . " should be not empty");
            }
        }
    }
    return $errors;
}
